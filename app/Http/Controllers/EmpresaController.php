<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\EmpresaRequest;
use Illuminate\Support\Facades\Storage;

class EmpresaController extends Controller
{

  use UploadTrait;

  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $empresa = Empresa::paginate(10);
    return view('empresa/index', ['empresa' => $empresa]);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    return view('empresa/create');
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(EmpresaRequest $request)
  {
    // Objeto del modelo Empresa
    $empresa = new Empresa;
    // Nombrar la imagen según la empresa
    $name = $request->input('nombre');
    // Establecer la ruta de almacenamiento
    $folder = '/uploads/images/';

    if ($request->file('logo') != null){
      // Establecer fuente de la imagen
      $image = $request->file('logo');
      // Crear la ruta donde irá la imagen renombrada
      $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
      // Si la imagen carga exitosamente, entonces cree el registro en la base de datos
      if ($this->uploadOne($image, $folder, 'public', $name)){

        $empresa->nombre = $name;
        $empresa->correo = $request->input('correo');
        $empresa->logo = $filePath;
        $empresa->url = $request->input('url');

        if ($empresa->save()){
          return redirect () -> back () -> with (['status' => 'Empresa creada correctamente.']);
        } else{
          Storage::delete($filePath);
          return redirect () -> back () -> with (['fail' => 'Ocurrió un error intentando almacenar la empresa en la base de datos. Contácte al administrador.']);
        }

        $validated = $request->validated();
      } else{
        return redirect () -> back () -> with (['fail' => 'Ocurrió un error almacenando la imagen. Contácte al administrador.']);
      }
    } else{
      $image = 'Nlogo.png';
      // Crear la ruta donde irá la imagen renombrada
      $filePath = $folder . $image;
      $empresa->nombre = $name;
      $empresa->correo = $request->input('correo');
      $empresa->logo = $filePath;
      $empresa->url = $request->input('url');

      if ($empresa->save()){
        return redirect () -> back () -> with (['status' => 'Empresa creada correctamente.']);
      } else{
        Storage::delete($filePath);
        return redirect () -> back () -> with (['fail' => 'Ocurrió un error intentando almacenar la empresa en la base de datos. Contácte al administrador.']);
      }

      $validated = $request->validated();
    }

  }

  /**
  * Display the specified resource.
  *
  * @param  \App\Empresa  $empresa
  * @return \Illuminate\Http\Response
  */
  public function show(Empresa $empresa)
  {
    return view('empresa/show', compact('empresa'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Empresa  $empresa
  * @return \Illuminate\Http\Response
  */
  public function edit(Empresa $empresa)
  {
    return view('empresa/update', compact('empresa'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Empresa  $empresa
  * @return \Illuminate\Http\Response
  */
  public function update(EmpresaRequest $request, Empresa $empresa)
  {
    $name = $request->input('nombre');
    // Recuperar nombre del logo anterior
    $old_img = $request->input('old_img');

    // Atributos del objeto Empresa
    $pib = array();
    $pib['nombre'] = $name;
    $pib['correo'] = $request->input('correo');
    $pib['url'] = $request->input('url');

    // Si no se cargó una imagen y la empresa no tenía logo
    if($request->file('logo') == null && $request->input('old_img') == '/uploads/images/Nlogo.png'){
      $pib['logo'] = '/uploads/images/Nlogo.png';
      // Si se realiza correctamente la actualización en la base de datos.
      if ($empresa->update($pib)){
        return redirect () -> back () -> with (['status' => 'Empresa actualizada correctamente.']);
        $validated = $request->validated();
      } else{
        return redirect () -> back () -> with (['fail' => 'Ocurrió un error intentando actualizar el registro en la base de datos. Contácte al administrador.']);
      }
    }

    // Si no se cargó una imagen y la empresa tenía logo
    if($request->file('logo') == null && $request->input('old_img') != '/uploads/images/Nlogo.png'){
      $pib['logo'] = $old_img;
      // Si se realiza correctamente la actualización en la base de datos.
      if ($empresa->update($pib)){
        return redirect () -> back () -> with (['status' => 'Empresa actualizada correctamente.']);
        $validated = $request->validated();
      } else{
        return redirect () -> back () -> with (['fail' => 'Ocurrió un error intentando actualizar el registro en la base de datos. Contácte al administrador.']);
      }
    }

    // Si se cargó una imagen y la empresa tenía logo.
    if($request->file('logo') != null && $request->input('old_img') != '/uploads/images/Nlogo.png') {
      //Si se elimina correctamente la imagen anterior, cargue la nueva.
      if (Storage::disk('public')->delete($old_img)){
        // Obtener imagen
        $image = $request->file('logo');
        // Establecer la ruta de almacenamiento
        $folder = '/uploads/images/';
        // Crear la ruta donde irá la imagen renombrada
        $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
        // Si la imagen carga exitosamente, entonces actualice el registro en la base de datos.
        if($this->uploadOne($image, $folder, 'public', $name)){
          // Asignar nombre con ruta de imagen al atributo logo
          $pib['logo'] = $filePath;
          // Si se realiza correctamente la actualización en la base de datos.
          if ($empresa->update($pib)){
            return redirect () -> back () -> with (['status' => 'Empresa actualizada correctamente.']);
            $validated = $request->validated();
          } else{
            return redirect () -> back () -> with (['fail' => 'Ocurrió un error intentando actualizar el registro en la base de datos. Contácte al administrador.']);
          }
        } else{
          return redirect () -> back () -> with (['fail' => 'Ocurrió un error intentando almacenar el nuevo logo. Contácte al administrador.']);
        }
      } else{
        return redirect () -> back () -> with (['fail' => 'Ocurrió un error intentando eliminar el logo anterior. Contácte al administrador.']);
      }
    }

  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Empresa  $empresa
  * @return \Illuminate\Http\Response
  */
  public function destroy(Empresa $empresa)
  {
    try {
      if ($empresa->logo != '/uploads/images/Nlogo.png'){
        if ($empresa->delete()){
          if (Storage::disk('public')->delete($empresa->logo)){
            return redirect () -> back () -> with (['status' => 'Empresa eliminada correctamente.']);
          } else{
            return redirect () -> back () -> with (['fail' => 'Ocurrió un error intentando eliminar el logo de la empresa. Contácte al administrador.']);
          }
        } else{
          return redirect () -> back () -> with (['fail' => 'Ocurrió un error intentando eliminar la empresa. Contácte al administrador.']);
        }
      } else {
        if ($empresa->delete()){
          return redirect () -> back () -> with (['status' => 'Empresa eliminada correctamente.']);
        } else{
          return redirect () -> back () -> with (['fail' => 'Ocurrió un error intentando eliminar la empresa. Contácte al administrador.']);
        }
      }
    } catch (Exception $e) {
        return redirect () -> back () -> with (['fail' => 'La empresa no puede ser eliminada debido a que varios empleados siguen vinculados a la misma.']);
        return false;
    }
  }
}
