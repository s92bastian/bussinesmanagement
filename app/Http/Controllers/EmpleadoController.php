<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Empleado;
use Illuminate\Http\Request;
use App\Http\Requests\EmpleadoRequest;
use Illuminate\Support\Facades\DB;

class EmpleadoController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $empleado = Empleado::paginate(10);
    return view('Empleado/index', ['empleado' => $empleado]);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $empresa = DB::table('empresa')->get();
    return view('Empleado/create', ['empresa' => $empresa]);
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(EmpleadoRequest $request)
  {
    Empleado::create($request->all());
    return redirect () -> back () -> with (['status' => 'Empleado creado correctamente.']);
    $validated = $request->validated();
  }

  /**
  * Display the specified resource.
  *
  * @param  \App\Empleado  $empleado
  * @return \Illuminate\Http\Response
  */
  public function show(Empleado $empleado)
  {
    return view('empleado/show', compact('empleado'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Empleado  $empleado
  * @return \Illuminate\Http\Response
  */
  public function edit(Empleado $empleado)
  {
    $empresa = DB::table('empresa')->get();
    return view('empleado/update', compact('empleado', 'empresa'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Empleado  $empleado
  * @return \Illuminate\Http\Response
  */
  public function update(EmpleadoRequest $request, Empleado $empleado)
  {
    $empleado->update($request->all());
    return redirect () -> back () -> with (['status' => 'Empleado actualizado correctamente.']);
    $validated = $request->validated();
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Empleado  $empleado
  * @return \Illuminate\Http\Response
  */
  public function destroy(Empleado $empleado)
  {
    $empleado->delete();
    return redirect () -> back () -> with (['status' => 'Empleado eliminado correctamente.']);
  }
}
