<?php

namespace App\Http\Requests;

use Egulias\EmailValidator\EmailValidator;
use Illuminate\Foundation\Http\FormRequest;
use Egulias\EmailValidator\Validation\RFCValidation;
use Egulias\EmailValidator\Validation\DNSCheckValidation;

class EmpresaRequest extends FormRequest
{
  /**
  * Determine if the user is authorized to make this request.
  *
  * @return bool
  */
  public function authorize()
  {
    return true;
  }

  /**
  * Get the validation rules that apply to the request.
  *
  * @return array
  */
  public function rules()
  {
    return [
      'nombre' => 'required',
      'correo' => 'email',
      'logo' => 'dimensions:min_width=100,min_height=100',
    ];
  }
}
