<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{

  /*
  Setting the table name
  */
  protected $table = 'empresa';

  /*
  Setting the primary key
  */
  protected $primaryKey = 'empresa_id';

  /*
  Setting the table columns
  */
  protected $fillable = ['nombre', 'correo', 'logo', 'url'];
}
