<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
  /*
  Setting the table name
  */
  protected $table = 'empleado';

  /*
  Setting the primary key
  */
  protected $primaryKey = 'empleado_id';

  /*
  Setting the primary key
  */
  protected $fillable = ['nombre', 'apellidos', 'empresa_id', 'correo', 'telefono'];

  /*
  * Relation with table empresa.
  */
  public function empresa(){
    return $this->belongsTo('App\Empresa', 'empresa_id');
  }
}
