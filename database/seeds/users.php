<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        'email' => 'admin@admin.com',
        'password' => bcrypt('contraseña'),
        'created_at' => date('Y-m-d H:m:s'),
      ]);

    }
}
