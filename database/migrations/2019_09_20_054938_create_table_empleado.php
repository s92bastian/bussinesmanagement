<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmpleado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleado', function (Blueprint $table) {
            $table->increments('empleado_id');
            $table->string('nombre')->nullable(false);
            $table->string('apellidos')->nullable(false);
            $table->integer('empresa_id')->unsigned();
            $table->string('correo');
            $table->string('telefono');
            $table->timestamps();
            $table->foreign('empresa_id')->references('empresa_id')->on('empresa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleado');
    }
}
