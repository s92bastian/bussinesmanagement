@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 col-md-offset-2">
          <table class="table table-striped">
            <tr>
              <td>ID</td>
              <td>NOMBRE</td>
              <td>CORREO</td>
              <td>LOGO</td>
              <td>URL</td>
            </tr>
            <tr>
              <td>{{ $empresa->id }}</td>
              <td>{{ $empresa->nombre }}</td>
              <td>{{ $empresa->correo }}</td>
              <td><img src="{{ $empresa->logo }}" alt="100" height="100"></td>
              <td>{{ $empresa->url }}</td>
            </tr>
        </table>
        </div>
    </div>
</div>
@endsection
