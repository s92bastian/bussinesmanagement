@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
          <a href="{{ route('empresa.create') }}" class="btn btn-primary" style="margin-bottom: 5px;">Crear nueva empresa</a>
          <table class="table table-striped">
            <tr>
              <td>ID</td>
              <td>NOMBRE</td>
              <td>CORREO</td>
              <td>LOGO</td>
              <td>URL</td>
              <td>ACCIONES</td>
            </tr>
            @foreach ($empresa as $empresas)
            <tr>
              <td>{{ $empresas->empresa_id }}</td>
              <td>{{ $empresas->nombre }}</td>
              <td>{{ $empresas->correo }}</td>
              <td><img src="{{ $empresas->logo }}" alt="100" height="100"></td>
              <td>{{ $empresas->url }}</td>
              <td>
                <a href="{{ route('empresa.show', $empresas->empresa_id) }}" class="btn btn-primary" style="margin-left: 4px;">Ver</a>
                <a href="{{ route('empresa.edit', $empresas->empresa_id) }}" class="btn btn-success">Editar</a>
                <form class="delete" action="{{ route('empresa.destroy', $empresas->empresa_id) }}" method="post" style="float: left;">
                  @csrf
                  @METHOD('DELETE')
                  <button type="submit" class="btn btn-danger" id="dlt" onclick="return confirm('Realmente desea eliminar la empresa?')">Eliminar</button>
                </form>
              </td>
            </tr>
            @endforeach
          </table>
          {{ $empresa->links() }}
          @if (session('status'))
              <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                  {{ session('status') }}
              </div>
          @endif
          @if (session('fail'))
              <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                  {{ session('fail') }}
              </div>
          @endif
        </div>
    </div>
</div>
@endsection
