@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Actualización de empresa</div>
                    <div class="card-body">
                      @if (session('status'))
                          <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                              {{ session('status') }}
                          </div>
                      @endif
                      @if (session('fail'))
                          <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                              {{ session('fail') }}
                          </div>
                      @endif
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    @if ($errors->any())
                                        <div class="alert alert-danger alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>
                                                        {{ $error }}
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <form action="{{ route('empresa.update', $empresa->empresa_id) }}" method="POST" role="form" enctype="multipart/form-data">
                                        @csrf
                                        @METHOD('PUT')
                                        <div class="form-group row">
                                            <label for="name" class="col-md-4 col-form-label text-md-right">Nombre</label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="nombre" id="nombre" value="{{ $empresa->nombre }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="email" class="col-md-4 col-form-label text-md-right">Correo</label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="correo" id="correo" value="{{ $empresa->correo }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="profile_image" class="col-md-4 col-form-label text-md-right">Logo</label>
                                            <div class="col-md-1">
                                                <img src="{{ $empresa->logo }}" style="width: 35px; height: 35px;">
                                            </div>
                                            <div class="col-md-5">
                                                <input type="file" class="form-control" name="logo" id="logo">
                                                <input type="text" class="form-control" name="old_img" id="old_img"  style="display: none;" value="{{ $empresa->logo }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="email" class="col-md-4 col-form-label text-md-right">Url</label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="url" id="url" value="{{ $empresa->url }}">
                                            </div>
                                        </div>
                                        <div class="form-group row mb-0 mt-5">
                                            <div class="col-md-8 offset-md-4">
                                                <button type="submit" class="btn btn-primary">Actualizar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
