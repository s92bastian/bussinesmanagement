@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Bienvenido.</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    Prueba técnica de conocimientos - Laravel.<br>
                    PUNTOS DE LA PRUEBA FUNCIONALES:<br>
                    * Migraciones para las tablas; empresas y empleados. [OK]<br>
                    * Seeders para la tabla usuarios. [OK]<br>
                    * Autentiticación de Laravel y eliminación de subscripción. [OK]<br>
                    * CRUD para empresas y empleados. [OK]<br>
                    * Almacenamiento de imagenes con un minimo de 100x100. [OK]<br>
                    * Utilización de helpers y validación de campos. [OK]<br>
                    * Paginación de Laravel. [OK]<br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
