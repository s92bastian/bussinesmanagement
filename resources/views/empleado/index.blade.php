@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
          <a href="{{ route('empleado.create') }}" class="btn btn-primary" style="margin-bottom: 5px;">Crear nuevo empleado</a>
          <table class="table table-striped">

            <tr>
              <td>ID</td>
              <td>NOMBRE</td>
              <td>APELLIDOS</td>
              <td>EMPRESA</td>
              <td>CORREO</td>
              <td>TELÉFONO</td>
              <td>ACCIONES</td>
            </tr>
            @foreach ($empleado as $emplead)
            <tr>
              <td>{{ $emplead->empleado_id }}</td>
              <td>{{ $emplead->nombre }}</td>
              <td>{{ $emplead->apellidos }}</td>
              <td>{{ $emplead->empresa->nombre}}</td>
              <td>{{ $emplead->correo }}</td>
              <td>{{ $emplead->telefono }}</td>
              <td>
                <a href="{{ route('empleado.show', $emplead->empleado_id) }}" class="btn btn-primary" style="margin-left: 4px;">Ver</a>
                <a href="{{ route('empleado.edit', $emplead->empleado_id) }}" class="btn btn-success">Editar</a>
                <form class="delete" action="{{ route('empleado.destroy', $emplead->empleado_id) }}" method="post" style="float: left;">
                  @csrf
                  @METHOD('DELETE')
                  <button type="submit" class="btn btn-danger" id="dlt" onclick="return confirm('Realmente desea eliminar el empleado?')">Eliminar</button>
                </form>
              </td>
            </tr>
            @endforeach
          </table>
          {{ $empleado->links() }}
          @if (session('status'))
              <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                  {{ session('status') }}
              </div>
          @endif
          @if (session('fail'))
              <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                  {{ session('fail') }}
              </div>
          @endif
        </div>
    </div>
</div>
@endsection
