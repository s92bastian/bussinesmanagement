@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 col-md-offset-2">
          <table class="table table-striped">
            <tr>
              <td>ID</td>
              <td>NOMBRE</td>
              <td>APELLIDOS</td>
              <td>EMPRESA</td>
              <td>CORREO</td>
              <td>TELÉFONO</td>
            </tr>
            <tr>
              <td>{{ $empleado->empleado_id }}</td>
              <td>{{ $empleado->nombre }}</td>
              <td>{{ $empleado->apellidos }}</td>
              <td>{{ $empleado->empresa->nombre }}</td>
              <td>{{ $empleado->telefono }}</td>
              <td>{{ $empleado->correo }}</td>
            </tr>
        </table>
        </div>
    </div>
</div>
@endsection
