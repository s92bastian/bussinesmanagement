@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Creación de empleado</div>
                    <div class="card-body">
                      @if (session('status'))
                          <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                              {{ session('status') }}
                          </div>
                      @endif
                      @if (session('fail'))
                          <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                              {{ session('fail') }}
                          </div>
                      @endif
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    @if ($errors->any())
                                        <div class="alert alert-danger alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>
                                                        {{ $error }}
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <form action="{{ route('empleado.update', $empleado->empleado_id) }}" method="POST" role="form" enctype="multipart/form-data">
                                        @csrf
                                        @METHOD('PUT')
                                        <div class="form-group row">
                                            <label for="nombre" class="col-md-4 col-form-label text-md-right">Nombres</label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="nombre" id="nombre" value="{{$empleado->nombre}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="apellidos" class="col-md-4 col-form-label text-md-right">Apellidos</label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="apellidos" id="apellidos" value="{{$empleado->apellidos}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="empresa_id" class="col-md-4 col-form-label text-md-right">Empresa</label>
                                            <div class="col-md-6">
                                                <select class="form-control" name="empresa_id" id="empresa_id">
                                                    <option value="{{ $empleado->empresa_id }}" selected> {{ $empleado->empresa->nombre }} </option>
                                                  @foreach ($empresa as $emp)
                                                    <option value="{{ $emp->empresa_id }}"> {{ $emp->nombre }} </option>
                                                  @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="correo" class="col-md-4 col-form-label text-md-right">Correo</label>
                                            <div class="col-md-6">
                                                <input type="email" class="form-control" name="correo" id="correo" value="{{$empleado->correo}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="telefono" class="col-md-4 col-form-label text-md-right">Teléfono</label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="telefono" id="telefono" value="{{$empleado->telefono}}">
                                            </div>
                                        </div>
                                        <div class="form-group row mb-0 mt-5">
                                            <div class="col-md-8 offset-md-4">
                                                <button type="submit" class="btn btn-primary">Guardar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
