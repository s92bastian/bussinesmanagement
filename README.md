# BussinesManagement

Prueba de conocimientos técnicos Laravel.

Como requisito previo, es necesario instalar XAMP o algún servidor local y crear una base de datos con el nombre:

* bussines_management

Es necesario tambien, instalar NodeJs y Composer. En la terminal, dirigirse a la ruta donde se clonó el proyecto y ejecutar el siguiente comando:

* npm run dev

Con el fin de generar los datos iniciales de la aplicación, es necesario ejecutar los siguientes comandos en la terminal:

* php artisan key:generate

* php artisan migrate

* php artisan db:seed
